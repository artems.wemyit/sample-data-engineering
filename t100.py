from argparse import ArgumentParser
from os.path import basename

import pandas as pd
from sqlalchemy import create_engine, text

import constants as cs
import helpers as hs


def process_raw_schedule(raw_schedule_df, acs_t100):
    result = raw_schedule_df.copy()

    result['Travel Month'] = pd.to_datetime(result['Time series'])
    result['Month'] = pd.DatetimeIndex(result['Travel Month']).month
    result['Travel Year'] = pd.DatetimeIndex(result['Travel Month']).year

    result['OtherSeats'] = 0
    result['PremEcon'] = 0

    result['Shared AL Des'] = result['Shared AL Des'].fillna(result['Carrier Code'])

    result.columns = cs.raw_schedule_cols
    result = result[cs.raw_schedule_cols_sorted]

    if acs_t100['MKT_AL'].isna().count() == len(acs_t100.index):
        print('Update vague aircraft types in SCHEDULE Only.')
        for col_name, replacement in cs.raw_schedule_mapping.items():
            result[col_name] = result[col_name].replace(replacement)
        print('Schedule updated with required fixes.')

    return result


def validate(acs_t100, raw_schedule):
    if acs_t100.empty:
        raise ValueError('ACS table is empty.')

    month_check = (
        acs_t100[acs_t100['MKT_AL'].isna()][['YEAR', 'MONTH']]
        .drop_duplicates()
        .set_index(['YEAR', 'MONTH'])
        .index
    )

    if len(month_check) > 12:
        raise ValueError(
            'Invalid t100 dataset. Only one year at a time can be accepted.'
        )

    months_in_schedule = raw_schedule.groupby(['Year', 'Month']).size()

    missing_entries_in_schedule = (
        pd.DataFrame(
            {'result': month_check.map(months_in_schedule).fillna(0)},
            index=month_check
        )
        .pipe(lambda df: df[df['result'].eq(0)].reset_index())
        [['YEAR', 'MONTH']]
    )

    if not missing_entries_in_schedule.empty:
        raise ValueError(
            'Schedule dates do not match t100 dates. Schedule missing for: \n {}'
            .format(missing_entries_in_schedule)
        )


def realign(acs_df):
    result = acs_df.copy()

    keys_align = ['ORIGIN', 'DEST', 'UNIQUE_CARRIER', 'YEAR', 'MONTH']

    misaligns = (
        result.pipe(lambda df: df[
            df['DEPARTURES_PERFORMED'].gt(0)
            & df['DEPARTURES_SCHEDULED'].eq(0)
        ])
        [[
            'AIRCRAFT_TYPE',
            'DEPARTURES_PERFORMED',
            'DEPARTURES_SCHEDULED',
            *keys_align
        ]]
    )

    realign = (
        result.pipe(lambda df: df[
            df['DEPARTURES_PERFORMED'].eq(0)
            & df['DEPARTURES_SCHEDULED'].gt(0)
        ])
        [[
            'AIRCRAFT_TYPE',
            'DEPARTURES_PERFORMED',
            'DEPARTURES_SCHEDULED',
            *keys_align
        ]]
        .groupby(keys_align)
        ['DEPARTURES_SCHEDULED']
        .apply(set)
    )

    misaligns['candidates'] = misaligns.set_index(keys_align).index.map(realign)
    if not misaligns['candidates'].notna().any():
        return result

    misaligns.loc[
        misaligns['candidates'].notna(),
        'departures_scheduled_updated'
    ] = (
        misaligns[misaligns['candidates'].notna()]
        .apply(hs.min_diff, axis='columns')
    )

    misaligns['departures_scheduled_realigned'] = (
        misaligns['departures_scheduled_updated']
        .combine_first(misaligns['DEPARTURES_SCHEDULED'])
    )

    result.loc[
        misaligns.index,
        'DEPARTURES_SCHEDULED'
    ] = misaligns['departures_scheduled_realigned']

    result = result[~result['DEPARTURES_PERFORMED'].eq(0)]

    without_scheduled_dep_filt = (
        result
        .pipe(lambda df: df[
            df['DEPARTURES_PERFORMED'].gt(0)
            & df['DEPARTURES_SCHEDULED'].eq(0)
        ])
        .index
    )

    result.loc[without_scheduled_dep_filt, 'DEPARTURES_SCHEDULED'] = (
        result.loc[without_scheduled_dep_filt, 'DEPARTURES_PERFORMED']
    )

    return result


def process_not_in_schedule(acs_df, raw_schedule):
    result = acs_df.copy()

    available_schedule_data = raw_schedule.set_index(['OpAl', 'Year', 'Month']).index.drop_duplicates()

    index_not_in_schedule = (
        result
        .reset_index()
        .set_index(['UNIQUE_CARRIER', 'YEAR', 'MONTH'])
        .pipe(lambda df: df[~df.index.isin(available_schedule_data)])
        ['index']
    )

    result.loc[index_not_in_schedule, 'DEBUG'] = '1 Not in schedule'
    result.loc[index_not_in_schedule, 'MKT_AL'] = result.loc[index_not_in_schedule, 'UNIQUE_CARRIER']

    return result


def process_one_distinct_marketing_carrier(acs_df, raw_schedule):
    result = acs_df.copy()

    one_marketing_carrier = (
        raw_schedule
        .groupby(['Year', 'Month', 'OpAl'])
        ['MktAl']
        .agg(['nunique', 'first'])
        .pipe(lambda df: df[df['nunique'].eq(1)])
        ['first']
    )

    return hs.apply_mapping(
        acs_df=result,
        keys=['YEAR', 'MONTH', 'UNIQUE_CARRIER'],
        mapping=one_marketing_carrier,
        debug_text='2 Unique carrier'
    )


def process_unique_orig_dest(acs_df, raw_schedule):
    result = acs_df.copy()

    keys = ['UNIQUE_CARRIER', 'ORIGIN', 'DEST', 'YEAR', 'MONTH']

    city_pairs = hs.get_empty_mktal(result)[keys].drop_duplicates().set_index(keys).index

    city_pairs_mapping = hs.create_unique_mapping(
        raw_schedule,
        ['OpAl', 'Orig', 'Dest', 'Year', 'Month'],
        city_pairs
    )

    return hs.apply_mapping(
        result,
        keys,
        city_pairs_mapping,
        '3 unique origin and destination'
    )


def process_unique_orig_dest_equip(acs_df, raw_schedule, aircraft_mapping):
    result = acs_df.copy()

    keys = ['UNIQUE_CARRIER', 'ORIGIN', 'DEST', 'AIRCRAFT_TYPE', 'YEAR', 'MONTH']

    city_pairs_air = pd.merge(
        hs.get_empty_mktal(result)[keys],
        aircraft_mapping,
        how='inner',
        left_on='AIRCRAFT_TYPE',
        right_on='DOT'
    ).set_index(['UNIQUE_CARRIER', 'ORIGIN', 'DEST', 'YEAR', 'MONTH', 'Innovata'])

    city_pairs_air_values = hs.create_unique_mapping(
        raw_schedule,
        ['OpAl', 'Orig', 'Dest', 'Year', 'Month', 'Equip'],
        city_pairs_air.index.drop_duplicates()
    )

    city_pairs_air['values'] = city_pairs_air.index.map(city_pairs_air_values)

    city_pairs_air_mapping = (
        city_pairs_air
        [city_pairs_air['values'].notna()]
        .reset_index()
        .set_index(keys)
        ['values']
    )

    return hs.apply_mapping(
        result,
        keys,
        city_pairs_air_mapping,
        '4 marketing code by origin, destination, and equipment'
    )


def process_for_split(acs_df, raw_schedule, aircraft_mapping):
    result = acs_df.copy()

    city_pairs_probable = (
        pd.merge(
            hs.get_empty_mktal(result).reset_index(),
            aircraft_mapping.rename(columns={'Innovata': 'Equip'}),
            left_on='AIRCRAFT_TYPE',
            right_on='DOT'
        )
        [[*result.columns, 'index', 'Equip']]
        .set_index(['UNIQUE_CARRIER', 'ORIGIN', 'DEST', 'YEAR', 'MONTH', 'Equip'])
    )

    dupes = (
        city_pairs_probable
        [city_pairs_probable.index.duplicated(keep=False)]
        .reset_index()
    )
    if not dupes.empty:
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            print(f'Duplicated city pairs:\n{dupes}')

    keys = ['OpAl', 'Orig', 'Dest', 'Year', 'Month', 'Equip']

    raw_schedule_probable = hs.filter_by_keys(
        raw_schedule,
        keys,
        city_pairs_probable.index
    ).astype({'Flights': float, 'Seats': float})

    nonunique_airlines = (
        raw_schedule_probable
        .groupby(keys)
        ['MktAl']
        .nunique()
        .gt(1)
        .pipe(lambda s: s[s]).index
    )
    raw_schedule_probable = hs.filter_by_keys(
        raw_schedule_probable.reset_index(),
        keys,
        nonunique_airlines
    )

    agg_functions = {
        'Flights': 'sum',
        'Seats': 'sum'
    }

    totals_probable = raw_schedule_probable.groupby(keys).agg(agg_functions)

    new_acs_values = (
        raw_schedule_probable
        .groupby([*keys, 'MktAl'])
        .agg(agg_functions)
        .reset_index()
        .set_index(keys)
        .pipe(lambda df: df.assign(**{
            f'total_{key}': df.index.map(totals_probable[key]) for key in agg_functions
        }))
        .pipe(lambda df: df.assign(**{
            f'perc_{key}': df[key] / df[f'total_{key}'] for key in agg_functions
        }))
    )

    columns_to_calculate = [
        'DEPARTURES_SCHEDULED',
        'DEPARTURES_PERFORMED',
        'PAYLOAD',
        'SEATS',
        'PASSENGERS',
        'FREIGHT',
        'MAIL',
        'RAMP_TO_RAMP',
        'AIR_TIME'
    ]

    new_acs = pd.merge(
        city_pairs_probable.reset_index(),
        new_acs_values[['MktAl', 'perc_Seats']].reset_index(drop=True),
        left_on=city_pairs_probable.index.to_numpy(),
        right_on=new_acs_values.index.to_numpy()
    ).pipe(
        lambda df: df.assign(MKT_AL=df['MktAl'], DEBUG='11 for split').assign(**{
            k: (df[k] * df['perc_Seats']).round() for k in columns_to_calculate
        })
    ).drop(columns=['key_0', 'MktAl', 'perc_Seats'])

    return result.drop(index=new_acs['index']).append(
        new_acs.drop(columns=['index', 'Equip']),
        ignore_index=True
    )


def process_airline_mapping(
    acs_df,
    raw_schedule,
    aircraft_mapping,
    key_mappings,
    debug_message
):
    result = acs_df.copy()

    def match_city_pairs(mapping):
        acs_unique_keys = list(mapping)
        raw_schedule_keys = [mapping[k] for k in acs_unique_keys]

        city_pairs_probable = (
            pd.merge(
                hs.get_empty_mktal(result).reset_index(),
                aircraft_mapping.rename(columns={'Innovata': 'Equip'}),
                left_on='AIRCRAFT_TYPE',
                right_on='DOT'
            )
            .set_index('index')
            [[*result.columns, 'Equip']]
        )

        raw_schedule_probable = hs.filter_by_keys(
            raw_schedule,
            raw_schedule_keys,
            city_pairs_probable.set_index(acs_unique_keys).index.drop_duplicates()
        ).reset_index()

        mapping_largest_opal_count = (
            raw_schedule_probable
            .groupby([*raw_schedule_keys, 'MktAl'])
            ['OpAl']
            .count()
            .sort_values(ascending=False)
            .rename('opal_count')
            .reset_index()
            .drop_duplicates(raw_schedule_keys)
            .set_index(raw_schedule_keys)
        )

        # multiple_mktal_equal_opal_count = (
        #     mapping_largest_opal_count
        #     .pipe(lambda df: df[df['opal_count'] > 1])
        #     .groupby(['MktAl', 'opal_count'])
        #     .filter(lambda df: df.shape[0] > 1)
        # )
        # if not multiple_mktal_equal_opal_count.empty:
        #     with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        #         print(f'\n2 or more MktAl have equal OpAl count:\n{multiple_mktal_equal_opal_count}')

        matched_city_pairs = (
            city_pairs_probable
            .pipe(lambda df: df.assign(**{
                k: df.set_index(acs_unique_keys).index.map(mapping_largest_opal_count[k])
                for k in ['MktAl', 'opal_count']
            }))
            .dropna(subset=['MktAl'])
            [['MktAl', 'opal_count']]
        )

        return matched_city_pairs

    matched_candidates = pd.concat(
        [match_city_pairs(m) for m in key_mappings],
    )

    matched_mapping = (
        matched_candidates
        .reset_index()
        .sort_values(['opal_count', 'index'], ascending=False)
        .drop_duplicates('index')
        .set_index('index')
        ['MktAl']
    )

    result.loc[matched_mapping.index, 'MKT_AL'] = matched_mapping
    result.loc[matched_mapping.index, 'DEBUG'] = debug_message

    return result


def calculate_leftovers(acs_df, aircraft_mapping):
    result = acs_df.copy()

    return (
        pd.merge(
            hs.get_empty_mktal(result).reset_index(),
            aircraft_mapping,
            left_on='AIRCRAFT_TYPE',
            right_on='DOT'
        )
        .rename(columns={
            'UNIQUE_CARRIER': 'Airline',
            'Innovata': 'Aircraft',
            'AIRCRAFT_TYPE': 'Aircraft Code',
        })
        .groupby(['YEAR', 'MONTH', 'Airline', 'Aircraft', 'Aircraft Code'])
        ['index']
        .count()
        .rename('Entries')
        .reset_index()
    )


def process_leftovers(acs_df):
    result = acs_df.copy()

    catch_all_entries = hs.get_empty_mktal(result).index

    result.loc[catch_all_entries, 'DEBUG'] = '9 catch all'
    result.loc[catch_all_entries, 'MKT_AL'] = result.loc[catch_all_entries, 'UNIQUE_CARRIER']

    return result


def setup_id(acs_df):
    result = acs_df.copy()

    id_columns = [
        'MKT_AL',
        'UNIQUE_CARRIER_ENTITY',
        'ORIGIN',
        'DEST',
        'CONFIG',
        'AIRCRAFT_TYPE',
        'YEAR',
        'MONTH',
        'CLASS'
    ]

    result['ID'] = (
        pd.Series('', index=result.index)
        .str.cat(result[id_columns].astype(str))
    )

    return result


def deduplicate(acs_df):
    result = acs_df.copy()

    duplicates_filter = result['ID'].duplicated(keep=False)
    duplicates = result[duplicates_filter]
    fields_to_calculate = {
        'DEPARTURES_SCHEDULED',
        'DEPARTURES_PERFORMED',
        'PAYLOAD',
        'SEATS',
        'PASSENGERS',
        'FREIGHT',
        'MAIL',
        'RAMP_TO_RAMP',
        'AIR_TIME',
    }
    agg_functions = {
        k: 'sum' if k in fields_to_calculate else 'first'
        for k in result.columns
    }

    new_values = (
        duplicates
        .groupby('ID')
        .agg(agg_functions)
        .pipe(
            lambda df: df.assign(**{
                k: df[k].round() for k in fields_to_calculate
            })
        )
    )
    result = result[~duplicates_filter].append(new_values, ignore_index=True)

    if not duplicates.empty:
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            print(f"Duplicates fixed:\n{duplicates.drop_duplicates()['ID']}")

    return result


def export_to_db(acs_df, args):
    result = acs_df.copy()

    engine = create_engine(args.db_uri)

    if engine.has_table(args.table_name):
        backup_name = f'{args.table_name}_bak'
        queries = [
            f'DROP TABLE IF EXISTS {backup_name};',
            f'CREATE TABLE {backup_name} like {args.table_name};',
            f'INSERT {backup_name} SELECT * FROM {args.table_name};',
            f'TRUNCATE TABLE {args.table_name};'
        ]

        for query in queries:
            print(f'  Executing "{query}" ...')
            engine.execution_options(autocommit=True).execute(text(query))

        print(f'Table "{args.table_name}" has been backed up to "{backup_name}" one.')

    types = {
        'ID': str,
        'MKT_AL': str,
        'DEBUG': int,
        'YEAR': int,
        'MONTH': int,
        'ORIGIN': str,
        'ORIGIN_CITY_MARKET_ID': int,
        'DEST': str,
        'DEST_CITY_MARKET_ID': int,
        'UNIQUE_CARRIER': str,
        'UNIQUE_CARRIER_ENTITY': str,
        'DISTANCE': int,
        'CLASS': str,
        'AIRCRAFT_TYPE': int,
        'CONFIG': int,
        'DEPARTURES_SCHEDULED': int,
        'DEPARTURES_PERFORMED': int,
        'PAYLOAD': int,
        'SEATS': int,
        'PASSENGERS': int,
        'FREIGHT': int,
        'MAIL': int,
        'RAMP_TO_RAMP': int,
        'AIR_TIME': int,
    }
    result['DEBUG'] = result['DEBUG'].str.split().str.get(0)

    result.astype(types).to_sql(
        name=args.table_name,
        con=engine,
        if_exists='append',
        index=False,
        chunksize=10000,
        method='multi'
    )
    print(f'Data has been saved to "{args.table_name}" table.')


def main():
    arg_parser = ArgumentParser(
        prog=basename(__file__),
        description='DOT analyzer',
        allow_abbrev=False,
    )

    arg_parser.add_argument(
        '--acs-uri',
        dest='acs_uri',
        type=str,
        required=True
    )

    arg_parser.add_argument(
        '--raw-schedule-uri',
        dest='raw_schedule_uri',
        type=str,
        required=True
    )

    arg_parser.add_argument(
        '--aircraft-mapping-uri',
        dest='aircraft_mapping_uri',
        type=str,
        required=True
    )

    arg_parser.add_argument(
        '--db-uri',
        dest='db_uri',
        type=str,
        default='mysql+pymysql://root@localhost/airlinedata'
    )

    arg_parser.add_argument(
        '--table-name',
        dest='table_name',
        type=str,
        default='acs_t100'
    )

    arg_parser.add_argument(
        '--output-dir',
        dest='output_dir',
        type=str,
        default='_output'
    )

    args = arg_parser.parse_args()

    acs_t100 = (
        pd
        .read_csv(args.acs_uri, sep='|', header=None, names=cs.acs_cols, index_col=False)
        .assign(ID=pd.NA, MKT_AL=pd.NA, DEBUG=pd.NA)
        .drop(columns=cs.acs_cols_to_drop)
    )

    print('\nStart analyzing ...\n')

    raw_schedule = pd.read_csv(args.raw_schedule_uri, dtype=str)

    raw_schedule = process_raw_schedule(raw_schedule, acs_t100)

    aircraft_mapping = pd.read_csv(args.aircraft_mapping_uri)

    validate(acs_t100, raw_schedule)

    print('\nRealign ...')
    acs_t100 = realign(acs_t100)

    print(f'\nTotal todos:\n{hs.get_todos(acs_t100)}')

    print('\nDEBUG 1.')
    print(
        "Find all airlines that aren't in the schedule",
        'and set the marketing airline equal to the operating airline.'
    )
    acs_t100 = process_not_in_schedule(acs_t100, raw_schedule)

    print(f'\nRemaining after removing airlines not in schedule:\n{hs.get_todos(acs_t100)}')

    print('\nDEBUG 2.')
    print("Find all routes with only one distinct marketing carrier")
    acs_t100 = process_one_distinct_marketing_carrier(acs_t100, raw_schedule)

    print(f'\nOperating Airlines with > 1 Marketing Code:\n{hs.get_todos(acs_t100)}')

    print('\nDEBUG 3.')
    print(
        'Set marketing airline by unique origin and destination;',
        'make sure only one MktAl per route and OpAl.'
    )
    acs_t100 = process_unique_orig_dest(acs_t100, raw_schedule)

    print(f'\nRemaining NULL Marketing Codes after matching cities and op carrier:\n{hs.get_todos(acs_t100)}')

    print('\nDEBUG 4.')
    print("Set marketing code by origin, destination, and equipment")
    acs_t100 = process_unique_orig_dest_equip(acs_t100, raw_schedule, aircraft_mapping)

    print(f'\nRemaining NULL Marketing Codes after Op Carrier and Fleet match:\n{hs.get_todos(acs_t100)}')

    print('\nDEBUG 11.')
    print("Find most probable marketing airline by city_pairs")
    acs_t100 = process_for_split(acs_t100, raw_schedule, aircraft_mapping)

    print(f'\nRemaining NULL Marketing Codes after Probable Flight Match:\n{hs.get_todos(acs_t100)}')

    print('\nDEBUG 5.')
    print('Split t100 entries where two marketing airlines share one operating airline on a market')
    key_mapping = [{
        'UNIQUE_CARRIER': 'OpAl',
        'ORIGIN': 'Orig',
        'DEST': 'Dest',
        'YEAR': 'Year',
        'MONTH': 'Month',
        'Equip': 'Equip',
    }]
    acs_t100 = process_airline_mapping(
        acs_t100,
        raw_schedule,
        aircraft_mapping,
        key_mapping,
        '5 entries where two marketing airlines share one operating airline on a market'
    )

    print(
        '\nRemaining NULL Marketing Codes after split t100 entries',
        'where two marketing airlines share one operating airline on a market:\n',
        hs.get_todos(acs_t100))

    print('\nDEBUG 6.')
    key_mapping = [
        {
            'ORIGIN': 'Orig',
            'UNIQUE_CARRIER': 'OpAl',
            'YEAR': 'Year',
            'MONTH': 'Month',
            'Equip': 'Equip',
        },
        {
            'DEST': 'Dest',
            'UNIQUE_CARRIER': 'OpAl',
            'YEAR': 'Year',
            'MONTH': 'Month',
            'Equip': 'Equip',
        },
    ]
    acs_t100 = process_airline_mapping(
        acs_t100,
        raw_schedule,
        aircraft_mapping,
        key_mapping,
        '6 split by total departures from either city for operating carrier and equipment'
    )

    print(
        '\nRemaining NULL Marketing Codes after split t100 entries',
        'split by total departures from either city for operating carrier and equipment:\n',
        hs.get_todos(acs_t100)
    )

    print('\nDEBUG 7.')
    key_mapping = [
        {
            'ORIGIN': 'Orig',
            'UNIQUE_CARRIER': 'OpAl',
            'YEAR': 'Year',
            'MONTH': 'Month',
        },
        {
            'DEST': 'Dest',
            'UNIQUE_CARRIER': 'OpAl',
            'YEAR': 'Year',
            'MONTH': 'Month',
        },
    ]
    acs_t100 = process_airline_mapping(
        acs_t100,
        raw_schedule,
        aircraft_mapping,
        key_mapping,
        '7 split by Operating Carrier and total departures, no equipment'
    )

    print(
        '\nRemaining NULL Marketing Codes after split t100 entries',
        'split by Operating Carrier and total departures, no equipment:\n',
        hs.get_todos(acs_t100)
    )

    print('\nDEBUG 8.')
    key_mapping = [{
        'UNIQUE_CARRIER': 'OpAl',
        'Equip': 'Equip',
        'YEAR': 'Year',
        'MONTH': 'Month',
    }]
    acs_t100 = process_airline_mapping(
        acs_t100,
        raw_schedule,
        aircraft_mapping,
        key_mapping,
        '8 most probable airline based on equipment and op/mkt airline relationship (no cities)'
    )

    print(
        '\nRemaining NULL Marketing Codes after split t100 entries',
        'split by most probable airline based on equipment',
        'and op/mkt airline relationship (no cities):\n',
        hs.get_todos(acs_t100)
    )

    print('\nDEBUG 9.')
    print('Catch-all - Whatever is left, just set it to the operating carrier')
    leftovers = calculate_leftovers(acs_t100, aircraft_mapping)

    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(f'\nRemaining NULL Marketing Codes after Probable Flight Match:\n{leftovers}')

    acs_t100 = process_leftovers(acs_t100)

    print(f'\nRemaining NULL after all changes:\n{hs.get_todos(acs_t100)}')

    print('\nSetting up "ID" column ...')
    acs_t100 = setup_id(acs_t100)

    print('\nDeduplicating ...')
    acs_t100 = deduplicate(acs_t100)

    print(f'\nDebug values:\n{acs_t100.DEBUG.value_counts()}')

    filename = f'{args.output_dir}/{args.table_name}.csv'
    print(f'\nSaving output to "{args.output_dir}" ...')
    acs_t100.to_csv(filename, index=False)
    print(f'Data has been saved to "{filename}".')

    print('\nSaving output to DataBase ...')
    export_to_db(acs_t100, args)

    print('\nDone.\n')


if __name__ == '__main__':
    main()
