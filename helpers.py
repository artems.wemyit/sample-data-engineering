def min_diff(row):
    available_diffs = {
        abs(row['DEPARTURES_PERFORMED'] - c): c
        for c in row['candidates']
    }
    return available_diffs[min(available_diffs.keys())]


def get_todos(acs_df):
    result_df = acs_df.copy()

    return (
        result_df
        [result_df['MKT_AL'].isna()]
        .groupby(['YEAR', 'MONTH'])
        ['UNIQUE_CARRIER']
        .size()
    )


def filter_by_keys(df_to_filter, keys, index_to_filter):
    result_df = df_to_filter.copy()

    return (
        result_df
        .set_index(keys)
        .pipe(lambda df: df[df.index.isin(index_to_filter)])
    )


def group_by_keys(df_to_group, keys, index_to_filter):
    result_df = df_to_group.copy()
    return (
        filter_by_keys(
            result_df,
            keys,
            index_to_filter
        )
        .groupby(keys)
    )


def create_unique_mapping(schedule_df, keys, index_to_filter):
    return (
        group_by_keys(schedule_df, keys, index_to_filter)['MktAl']
        .agg(['nunique', 'first'])
        .pipe(lambda df: df[df['nunique'].eq(1)])
        ['first']
    )


def get_empty_mktal(acs_df):
    result_df = acs_df.copy()

    return result_df[result_df['MKT_AL'].isna()]


def apply_mapping(acs_df, keys, mapping, debug_text):
    result_df = acs_df.copy()

    tmp_df = (
        get_empty_mktal(result_df)
        .reset_index()
        [[*keys, 'index']]
        .set_index(keys)
        .pipe(lambda df: df.assign(value=df.index.map(mapping)))
        .dropna(subset=['value'])
    )
    result_df.loc[tmp_df['index'], 'MKT_AL'] = tmp_df['value'].values
    result_df.loc[tmp_df['index'], 'DEBUG'] = debug_text
    return result_df
